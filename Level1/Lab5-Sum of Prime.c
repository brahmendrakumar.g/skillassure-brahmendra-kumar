#include <stdio.h>

int main() 
{
   int a, b, c, d,sum = 0;
   printf("Enter two numbers: ");
   scanf("%d %d", &a, &b);
   printf("Prime numbers between %d and %d are: ", a, b);

   
   while (a <= b)
   {
      d = 0;
      if (a <= 1) 
      {
         ++a;
         continue;
      }

      for (c = 2; c <= a / 2; ++c) 
      {

         if (a % c == 0) 
         {
            d = 1;
            break;
         }
      }

      if (d == 0)
         {
            printf("%d ", a);
            sum = a + sum;
         }
      ++a;
   }
     printf("\n The sum of prime numbers is: %d ", sum);
   return 0;
}