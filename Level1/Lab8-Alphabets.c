#include <stdio.h>

 
int main()
{
  	char Str[100];
  	int i;
 
  	printf("\n Enter the string:  ");
  	scanf("%[^\n]s",Str);
  	
  	for (i = 0; Str[i]!='\0'; i++)
  	{
  		if(Str[i] >= 'A' && Str[i] <= 'Z')
  		{
  			Str[i] = Str[i] + 32;
		}
		else if(Str[i]>='a' && Str[i]<='z')
		{
		    Str[i] = Str[i] - 32;
		}
		
  	}

  	printf("\n The required string is: %s", Str);
  	
  	return 0;
}