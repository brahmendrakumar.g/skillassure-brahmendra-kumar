#include <stdio.h>

int main()
{
    //step1 = declaration of variables
    int num1, num2, temp;
 //step2 = accept the values from console
    printf("Enter the numbers");
    scanf("%d%d",&num1, &num2);
     printf("\n The numbers are : %d,%d", num1, num2);
    //Step3 = Calculation
    temp =num1;
    num1 = num2;
    num2 = temp;
   //step4 = display the output
   printf("\n The Swapped numbers are : %d,%d", num1, num2);
    return 0;
    
}


