#include <stdio.h>

int main()
{
    //step1: declaration
    int number=0,sum=0,i;
    printf("\n enter the numbers:");
    
    //step2: Accept the console
    scanf("%d",&number);
    
    //step3: calculation
    for(i=0;i<=number;i++)
    {
        if(i%2 !=0)
         sum = sum+i;
    }
    //step4: display the output
    printf("\n The sum of odd numbers is : %d",sum);
    return 0;
}